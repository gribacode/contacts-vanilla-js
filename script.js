const inputField = document.getElementById("inputField");
const list = document.getElementById("list");
const listItem = list.querySelectorAll("li.collection-item");

function filterList() {
  let inputFieldValue = inputField.value.toUpperCase();

  for (let i = 0; i < listItem.length; i++) {
    let link = listItem[i].getElementsByTagName("a")[0];
    if (link.innerHTML.toUpperCase().indexOf(inputFieldValue) > -1) {
      listItem[i].style.display = "";
    } else {
      listItem[i].style.display = "none";
    }
  }
}

inputField.addEventListener("keyup", () => {
  filterList();
});
